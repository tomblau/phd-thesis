%!TEX root = ../thesis.tex
% ******************************* Thesis Appendix A ****************************
\chapter{Proof of Theorem~\ref{thm:04:rrt} - Probabilistic Completeness of R3L}
\label{chap:appA}

This appendix proves that planning using RRT under differential constraints is probabilistically complete, as claimed in~\chapref{r3l}. The definitions of probabilistic completeness and of the differential constraints are reproduced here for readability

\begin{mydef}
A probabilistically complete planner finds a feasible solution (if one exists) with a probability approaching 1 in  the limit of infinite samples. 
\end{mydef}

\begin{equation}\label{eq:A:diff_cont}
\dot{s} = f(s(t), a(t)), \quad s(t) \in \mathcal{F}, \quad a(t) \in \mathcal{A}.
\end{equation}


The following proof is a modification of Theorem 2 from \citet{kleinbort2019}, where completeness of RRT in the RL setting is maintained without the need to explicitly sample a duration for every action.

~\eqref{A:diff_cont} defines the environment's differential constraints. In practice, to evaluate~\eqref{A:diff_cont} the interval $[0,t_{\tau}]$ is divided into $l >> n_{\tau}$ equal time intervals of duration $h$ such that $t_{\tau}=l\cdot h$. The differential constraints are then approximated by an Euler integration step, where the transition between consecutive time steps is given by:
\begin{equation}\label{eq:04:Euler}
    \begin{aligned}
    &s_{n+1} = s_{n}+f(s_n, a_n) \cdot h, \qquad s_{n},s_{n+1}\in \tau,\\
    &s.t. \lim\limits_{l \rightarrow \infty, h \rightarrow 0} \norm{s_n - \tau(n \cdot h)} = 0.
    \end{aligned}
\end{equation}

Assume that the planning environment is Lipschitz continuous in both state and action, constraining the rate of change of~\eqref{04:Euler}. More formally, there exist two positive constants $K_s, K_a >0$, such that $\forall s_0,s_1 \in \mathcal{F}, a_0,a_1\in \mathcal{A}:$
\begin{align} 
\norm{f(s_0, a_0)-f(s_0, a_1)} &\leq K_a\norm{a_0-a_1}, \\ 
\norm{f(s_0, a_0)-f(s_1, a_0)} &\leq K_s\norm{s_0-s_1}.
\end{align}

\begin{lemma}\label{lemma:sup:traj_similarity}
Suppose there exist two trajectories $\tau$, $\tau'$, where $s_0=\tau(0)$ and $s'_0=\tau'(0)$ such that $\norm{s_0-s'_0} \leq \delta_s$ with $\delta_s$ a positive constant. Suppose further that for each trajectory a piecewise constant action is applied, so that $\Upsilon(t) =a$ and $\Upsilon'(t) =a'$ is fixed during a time period $T \geq 0$. Then 
$\norm{\tau(T)-\tau'(T)} \leq e^{K_sT}\delta_s + TK_a e^{K_sT}\norm{a-a'}$.
\end{lemma}
The proof for Lemma~\ref{lemma:sup:traj_similarity} is given in Lemma 2 of \citet{kleinbort2019}. Intuitively, this bound is derived from compounding worst-case divergence between $\tau$ and $\tau'$ at every Euler step along $T$ which leads to an overall exponential dependence. 

Lemma~\ref{lemma:sup:traj_similarity} will provide the basis for a lower bound on the probability of choosing an action that will expand the tree successfully. Note that this scenario assumes that actions are drawn uniformly from $\mathcal{A}$, i.e. there is no steering function \footnote{The function $steer: \mathcal{S}\times\mathcal{S} \rightarrow \mathcal{A}$ returns an action $a_{steer}$ given two states $s{rand}$ and $s_{near}$ such that $a_{steer} = \argmin_{a \in \mathcal{A}}\norm{s_{rand}-(s_{near}+\Delta t \cdot f(s_{near}, a))} \: s.t. \: \norm{\Delta t \cdot f(s_{near}, a)} < \eta$, for a prespecified $\eta > 0$ \cite{Karaman2011}.}. When better estimations of the steering function are available, as described in~\secref{04:adapting_rrt_to_rl}, the performance of RRT significantly improves.

Let $\mathcal{B}_r(s)$ be a ball with a radius $r$ centered at $s$ for any given state $s \in \mathcal{S}$.


\begin{mydef}
A trajectory $\tau$ is defined as $\delta$-clear if for some $\delta_{clear} > 0$, it holds that $\mathcal{B}_{\delta_{clear}}(\tau(t)) \subset \mathcal{F}$ for all $t \in [0,t_\tau].$
\end{mydef}

\begin{lemma}\label{lemma:sup:pick_action}
Suppose that $\tau$ is a valid trajectory from $\tau(0)=s_0$ to $\tau(t_\tau)=s_{goal}$ with a duration of $t_\tau$ and a clearance of $\delta$. Without loss of generality, assume that actions are fixed for all $t\in [0,t_\tau]$, such that $\Upsilon(t)=a \in \mathcal{A}$.

Suppose further that RRT expands the tree from a state $s'_{0} \in \mathcal{B}_{(\kappa\delta-\epsilon)}(s_0)$ to a state $s'_{goal}$, for any $\kappa \in (0,1]$ and $\epsilon \in (0,\kappa \delta)$ the following bound holds:
\begin{equation}
    \Pr[s'_{goal} \in \mathcal{B}_{\kappa \delta}(s_{goal}))] \geq \frac{\zeta_{\abs{\mathcal{S}}} \cdot \frac{\kappa\delta-e^{K_s t_\tau}(\kappa\delta-\epsilon)}{K_a t_\tau e^{K_s t_\tau}}}{\abs{\mathcal{A}}}.     
\end{equation}
Here, $\zeta_{\abs{\mathcal{S}}} =\abs{\mathcal{B}_{1}(\cdot)}$ is the Lebesgue measure for a unit circle in $\mathcal{S}$.
\end{lemma}
\begin{proof}
Let $\tau'$ be a trajectory that starts from $s'_{0}$ and is expanded with an applied random action $a_{rand}$. According to Lemma~\ref{lemma:sup:traj_similarity}, 
\begin{align}
    \norm{\tau(t)-\tau'(t)}
    &\leq e^{K_st}\delta_s + tK_a e^{K_st}\norm{a-a_{rand}} \\
    &\leq e^{K_st}(\kappa\delta-\epsilon) + tK_a e^{K_st}\norm{a-a_{rand}}, \qquad \forall t \in [0,t_\tau],
\end{align}
where $\delta_s \leq \kappa\delta-\epsilon$ since $s'_{0} \in \mathcal{B}_{\kappa\delta-\epsilon}(s_0)$. The goal is to find $\norm{a-a_{rand}}$ such that the distance between the goal points of these trajectories, in the worst-case scenario, is bounded:
\begin{equation}
e^{K_s t_\tau}(\kappa\delta-\epsilon) + t_\tau K_a e^{K_st_\tau}\norm{a-a_{rand}} < \kappa \delta.
\end{equation}
Rearranging this formula yields a bound for $\norm{a-a_{rand}}$:
\begin{equation}
 \Delta a = \norm{a-a_{rand}} < \frac{\kappa \delta - e^{K_s t_\tau}(\kappa\delta-\epsilon)}{t_\tau K_a e^{K_st_\tau}}.
\end{equation}
Assuming that $a_{rand}$ is drawn out of a uniform distribution, the probability of choosing the proper action is 
\begin{equation}\label{eq:A:p_a_limit}
    p_a = \frac{\zeta_{\abs{\mathcal{S}}} \cdot \frac{\kappa \delta - e^{K_s t_\tau}(\kappa\delta-\epsilon)}{t_\tau K_a e^{K_st_\tau}}}{\abs{\mathcal{A}}},
\end{equation}
where $\zeta_{\abs{\mathcal{S}}}$ is used to account for the degeneracy in action selection due to the dimensionality of $\mathcal{S}$. Note that $\epsilon \in (0,\kappa \delta)$ guarantees $p_a \geq 0$, therefore $p_a$ is a valid probability.
\\
\end{proof}


~\eqref{A:p_a_limit} provides a lower bound for the probability of choosing the suitable action. The following lemma provides a bound on the probability of randomly drawing a state that will expand the tree toward the goal.
\begin{lemma}\label{lemma:transition}
Let $s \in \mathcal{S}$ be a state with clearance $\delta$, i.e. $\mathcal{B}_{\delta}(s) \subset \mathcal{F}$. Suppose that for an RRT tree $\mathbb{T}$ there exist a vertex $v \in \mathbb{T}$ such that $v \in \mathcal{B}_{2\delta/5}(s)$. Following the definition in~\secref{04:rrt_exploration}, denote $s_{near} \in \mathbb{T}$ as the closest vertex to $s_{rand}$. Then, the probability that $s_{near} \in \mathcal{B}_{\delta}(s)$ is at least $\abs{\mathcal{B}_{\delta/5}(s)}/\abs{S}$.
\end{lemma}
\begin{proof}
Let $s_{rand} \in \mathcal{B}_{\delta/5}(s)$. Therefore the distance between $s_{rand}$ and $v$ is upper-bounded by $\norm{s_{rand}-v} \leq  3\delta/5$. If there exists a vertex $s_{near}$ such that $s_{near} \neq v$ and $\norm{s_{rand}-s_{near}} \leq \norm{s_{rand}-v}$, then $s_{near} \in \mathcal{B}_{3\delta/5}(s_{rand}) \subset	\mathcal{B}_{\delta}(s)$. Hence, by choosing $s_{rand} \in \mathcal{B}_{\delta/5}(s)$, it is guaranteed that $s_{near} \in \mathcal{B}_{\delta}(s)$. As $s_{rand}$ is drawn uniformly, the probability for $s_{rand} \in \mathcal{B}_{\delta/5}(s)$ is $\abs{\mathcal{B}_{\delta/5}(s)}/\abs{S}$.    
\\
\end{proof}

We can now prove the main theorem.
\begin{customthm}{4.1}
	 Suppose that there exists a valid trajectory $\tau$ from $s_0$ to $\mathcal{F}_{goal}$ as defined in~\defref{04:traj}, with a corresponding piecewise constant control. The probability that R3L exploration fails to reach $\mathcal{F}_{goal}$ from $s_0$ after $k$ iterations is bounded by $a e^{-bk}$, for some constants $a,b > 0$.
\end{customthm}
\begin{proof}
Lemma~\ref{lemma:sup:pick_action} puts a bound on the probability to find actions that expand the tree from one state to another in a given time. As it has already been assumed that a valid trajectory exists, the probability defined in Lemma~\ref{lemma:sup:pick_action} is non-zero, i.e. $p_a > 0$, hence:
\begin{equation}\label{eq:A:time_limit}
    \kappa \delta - e^{K_s \Delta t}(\kappa\delta-\epsilon) > 0,
\end{equation}
where the parameters are set to $\kappa = 2/5$ and $\epsilon = 5^{-2}$ as was also done in~\citet{kleinbort2019}. It is further required that $\Delta t$, which is typically defined as an RL environment parameter, is chosen accordingly so as to ensure that~\eqref{A:time_limit} holds, i.e. $K_s \Delta t < \log\left(\frac{\kappa \delta}{\kappa \delta - \epsilon}\right)$.

The next step is to cover $\tau$ with balls of radius $\delta = \min\{\delta_{goal}, \delta_{clear}\}$, where $\mathcal{B}_{\delta_{goal}} \subseteq \mathcal{F}_{goal}$. The balls are spaced equally in time with the center of the $i^{th}$ ball being $c_i = \tau(\Delta t \cdot i), \forall i=0:m$, where $m=t_\tau/\Delta t$. Therefore, $c_0= s_0$ and $c_m=s_{goal}$. Consider the probability of the RRT propagating along $\tau$. Suppose that there exists a vertex $v \in \mathcal{B}_{2\delta/5}(c_i)$, we need to bound the probability $p$ that by taking a random sample $s_{rand}$, there will be a vertex $s_{near} \in \mathcal{B}_{\delta}(c_i)$ such that $s_{new} \in \mathcal{B}_{2\delta/5}(c_{i+1})$. Lemma~\ref{lemma:transition} provides a lower bound for the probability that $s_{near} \in \mathcal{B}_{\delta}(c_i)$, given that there exists a vertex $v \in \mathcal{B}_{2\delta/5}(c_i)$. This lower bound is $\abs{\mathcal{B}_{\delta/5}(s)}/\abs{S}$. Lemma~\ref{lemma:sup:pick_action} provides a lower bound for the probability of choosing an action from $s_{near}$ to $s_{new}$. This bound is $\rho \equiv  \frac{\zeta_{\abs{\mathcal{S}}} \cdot \frac{\kappa \delta - e^{K_s \Delta t}(\kappa\delta-\epsilon)}{\Delta t K_a e^{K_s \Delta t}}}{\abs{\mathcal{A}}} > 0$, where $t_\tau$ has been substituted with $\Delta t$. Consequently, $p \geq (\abs{\mathcal{B}_{\delta/5}(s)}\cdot \rho)/\abs{S}$.

For RRT to recover $\tau$, the transition between consecutive circles must be repeated $m$ times. This stochastic process can be described as a binomial distribution, where after performing $k$ trials (randomly choosing $s_{rand}$), there are $m$ successes (transitions between circles), and the transition success probability is $p$. The probability mass function of a binomial distribution is 
$\Pr(X=m)= \Pr(m;k,p) = \binom{k}{m}p^{m}(1-p)^{k-m}$.
The probability of failure (i.e. the process was unable to perform $m$ steps) is upper bounded by the cumulative distribution function (CDF) of the above binomial:
\begin{equation}
    \Pr(X < m)=\sum_{i=0}^{m-1}\binom{k}{i}p^{i}(1-p)^{k-i}.
\end{equation}
Using Chernoff's inequality one can derive the tail bounds of the CDF when $m \leq p\cdot k$:
\begin{align}
    \Pr(X < m) &\leq \exp \left(-\frac {1}{2p}\frac{(kp-m)^{2}}{k}\right)\\
    &=\exp \left(-\frac {1}{2}kp+m-\frac{m^{2}}{kp}\right)\\
    &\leq e^{m}e^{-\frac {1}{2}pk} = ae^{-bk}. \label{eq:A:greta}
\end{align}
In the other case, where $p < m/k < 1$, the upper bound is given by \citet{Arratia1989}: 
\begin{align}\label{eq:A:bound2}
    \Pr(X < m) &\leq \exp \left(-k\mathcal{D}\left({\frac {m}{k}}\parallel p\right)\right),
\end{align}
where $\mathcal{D}$ is the relative entropy such that 
\begin{equation*}
    D\left({\frac {m}{k}}\parallel p\right) = \frac{m}{k}\log {\frac {\frac {m}{k}}{p}}+(1-\frac {m}{k})\log {\frac {1-\frac {m}{k}}{1-p}}.
\end{equation*}
Rearranging $\mathcal{D}$,~\eqref{A:bound2} can be rewritten as follows:
\begin{align}
    \Pr(X < m) &\leq \exp \left(-k \left({\frac{m}{k}}\log\left(\frac{m}{kp}\right)+\frac{k-m}{k}\log\left({\frac {1-\frac{m}{k}}{1-p}}\right)\right)\right)\\
    &= \exp\left(-m\log\left(\frac{m}{kp}\right)\right)\exp\left(-k\log\left(\frac {1-\frac{m}{k}}{1-p}\right)\right)\exp\left(m\log\left(\frac {1-\frac{m}{k}}{1-p}\right)\right)\\
    &=\exp\left(-m\log\left(\frac{m(1-p)}{kp(1-\frac{m}{k})}\right)\right)\exp\left(-k\log\left(\frac {1-\frac{m}{k}}{1-p}\right)\right) \\
    & \leq \exp\left(-k\log\left(\frac {1-\frac{m}{k}}{1-p}\right)\right) \label{eq:A:jenny}\\
    & \leq \exp\left(-k\log\left(\frac {0.5}{1-p}\right)\right) \label{eq:A:david}\\
    & \leq e^{-kp} = ae^{-bk}\label{eq:A:zupko},
\end{align}
where~\eqref{A:jenny} is justified for the worst-case scenario where $p=m/k$, and~\eqref{A:david} uses the fact that $p < m/k < 0.5$, hence $ 1- m/k > 0.5$. In the last step, \eqref{A:zupko} is derived from the first term of the Taylor expansion of $\log\left(\frac{1}{1-p}\right) = \sum_{j=1}^{\infty} \frac{p^j}{j}$.

As $p$ and $m$ are fixed and independent of $k$, the expression for $\Pr(X <m)$ decays to zero exponentially with $k$, therefore RRT is probabilistically complete. 
\\
\end{proof}


It is worth noting that the failure probability $\Pr(X <m)$ depends on problem-specific properties, which give rise to the values of $a$ and $b$. Intuitively, $a$ depends on the scale of the problem such as volume of the goal set $\abs{\mathcal{F}^{RL}_{goal}}$ and how complex and long the solution needs to be, as evident in~\eqref{A:greta}. More importantly, $b$ depends on the probability $p$. Therefore, it is a function of the dimensionality of $\mathcal{S}$ (through the probability of sampling $s_{rand}$) and other environment parameters such as clearance (defined by $\delta$) and dynamics (via $K_s$, $K_a$), as specified in~\eqref{A:p_a_limit}.
