%!TEX root = ../thesis.tex
%*******************************************************************************
%*********************************** First Chapter *****************************
%*******************************************************************************

\chapter{Introduction}  %Title of the First Chapter
\label{chap:introduction}

\ifpdf
    \graphicspath{{Chapter1/Figs/Raster/}{Chapter1/Figs/PDF/}{Chapter1/Figs/}}
\else
    \graphicspath{{Chapter1/Figs/Vector/}{Chapter1/Figs/}}
\fi


%********************************** %First Section  **************************************
\section{Motivation} %Section - 1.1 

The history of technological advancement is one of iteratively making new tools to increase human productivity. In the $20^{th}$ century, this took on the form of off-loading human and animal labour to machines. For this process to continue, there is a growing need for machines that are autonomous, capable not just of exerting force in pre-determined patterns, but also of making complex decisions and reacting to their environment. Such robotic agents must manipulate themselves and their environment to achieve a specified result, such as putting a box on a shelf. Planning and control algorithms are popular solutions for this class of problem, but rely on knowledge of the system dynamics. In practice, the dynamics of the environment are often not known, or are otherwise not feasible to specify. Without the ability to reliably determine the consequences of an agent's action ahead of time, planning becomes ineffective. Complicating matters further, some elements in the environment are outside the agent's control, and their state may change independently of any action it performs.

The difficulty in defining dynamics a-priori has lead to the rise of algorithms that learn policies for agent behaviour from experience. Reinforcement Learning has emerged as one of the premier methodologies for solving such problems. In recent years, RL algorithms have managed to produce remarkable results in robotic control~\cite{tan18sim, zhu18reinforcement, rajeswaran18learning, amiranashvili18motion} as well as in playing games~\cite{burda2019exploration, alphago, alphastar, OpenAI_dota}, all without access to any knowledge about system dynamics.


\begin{figure}[t]
    \centering

    \subcaptionbox{Montezuma's Revenge}{\includegraphics[width=0.49\textwidth]{Figs/montezuma}} \hfill
    \subcaptionbox{World Rally Championship}{\includegraphics[width=0.49\textwidth]{Figs/wrc6}} \hfill\\
    \subcaptionbox{Dexterous Manipulation}{\includegraphics[width=0.49\textwidth]{Figs/dexterous}} \hfill
    \subcaptionbox{Minitaur Locomotion}{\includegraphics[width=0.49\textwidth]{Figs/locomotion}} \hfill

    \caption[Contemporary RL]{Contemporary tasks learned with RL. (a) Playing Montezuma's Revenge
        (courtesy of the Arcade Learning Environment~\cite{bellemare13arcade}). (b) Driving a realistic car simulation (Valeo/Inria~\cite{perot2017end}) (c) Object manipulation with a dexterous hand (OpenAI~\cite{andrychowicz19dexterous}). (d) Quadrupedal locomotion on uneven terrain (Google AI/UC Berkeley~\cite{haarnoja2019walk}).} 
\end{figure}


In the reinforcement learning setting, an agent is placed in an environment initialised to some state. The agent observes the environment and executes some action according to its policy. This action causes the state of the system to evolve, emitting a new observation as well as a scalar reward signal. This process repeats iteratively until some time threshold is exceeded constituting one episode, and the resulting sequence of states and actions is one trajectory. Over many episodes the policy is updated to try and maximise the expected total reward for an episode. Hence, reinforcement learning is a complex optimisation problem where the optimal action at each timestep depends on the behaviour of the policy at every future time step.

The above formulation, though flexible and powerful, leads to a number of challenges. Since the reward signal is the only information guiding optimisation, the policy learned by an RL algorithm will be one that (locally) maximises the reward. The reward signal must therefore be designed such that it is maximised by a policy that solves the task at hand. This is not trivial to do, and in practice RL agents sometimes learn to exploit a flaw in the reward signal instead of achieving the desired behaviour~\cite{everittreinforcement}.

One naive way of solving this problem is by using a sparse, binary reward indicating only success or failure. While this eases the burden on the practitioner, it introduces new difficulties. In all but the most trivial tasks, a newly initialised policy will only achieve success through random chance, and the probability of this happening is exceedingly low. When the agent does not achieve success, all the rewards have the same failure value, and there is no information to guide the policy towards better performance. In this case, most RL algorithms are reduced to a random walk in the space of policy parameters.

Another challenge facing RL algorithms is exploration of the state space. In order to achieve a single success, an agent must find a path in state space from an initial state to a goal state. In general, goal states are a vanishingly small subset of the state space, and the complexity of finding them by exhaustive search is exponential in the number of state dimensions. Furthermore, it is not possible to simply transition from the current state to any arbitrary state. Possible transitions are determined by environment dynamics and the set of possible actions. Since dynamics are not known a-priori in the RL setting, even getting from a known starting state to a known goal state is a difficult problem.

Finally, an RL algorithm must manage the trade-off between exploration and exploitation. On the one hand, an agent needs to explore the state space in order to find better trajectories and improve its policy. On the other hand, taking actions to explore the state space means sacrificing a known reward in regions that have already been explored. Since there is no way of knowing whether the current policy is optimal or not, it is difficult to balance between exploiting the available information and paying a price to gather new information which may or may not be useful.

The above challenges manifest themselves as inefficiencies in the learning process, meaning that algorithms must run for millions of iterations before an optimal policy is learned. State-of-the-art results often rely on large-scale hardware setups that run for many processor-hours~\cite{levine18learning, alphastar}. If reinforcement learning is to be the next frontier in automating human labour, this inefficiency must be addressed.



%********************************** %Second Section  *************************************
\section{Problem Statement} %Section - 1.2

The central problem that this thesis seeks to address is \emph{how to efficiently learn a control policy that optimally solves a task defined by a reward signal}. In particular, the interest is in realistic problems where controls are continuous, state spaces are large and high-dimensional, rewards are sparse, and system dynamics are unknown. Efficiency is defined in terms of the number of interactions with the environment, i.e. observation-action pairs, as this typically dominates runtime. 

Achieving optimality requires simultaneously solving a number of sub-problems: exploring the state space to discover goal states, learning the transition dynamics, and composing a policy that can traverse from arbitrary points in the state space to goal states in a minimum number of actions. Achieving efficiency involves solving two further problems: deciding where to explore, and balancing exploration with exploitation.

In practice, the overarching learning task can be decomposed into two stages. The first stage consists of finding the first few successful trajectories, regardless of optimality. In this stage the overwhelming majority of rewards are negative, and provide no information that can help direct policy learning. The second stage begins when the policy can reliably achieve success in a small but not negligible percentage of episodes (e.g. $10\%$). This stage consists of refining the policy to increase success rate and shorten average trajectory length. In this stage, the reward signal provides rich gradient information which the algorithm can use.


%********************************** % Third Section  *************************************
\section{Contributions}  %Section - 1.3 

This thesis addresses some of the sources of inefficiency in the RL settings by combining state-of-the-art reinforcement learning algorithms with techniques from other fields, such as planning and Bayesian inference. The main contributions are described in the sequel.

\subsection{Regularisation of Pre-training using Variational Inference}
In the sparse reward setting, most trajectories yield no positive reward, and for most policy parameterisations the reward surface (with respect to policy parameters) has no gradient. Therefore, until an RL agent has managed to find the first few successful trajectories, its search through the state space is little more than a random walk. A popular strategy for side-stepping this stage of the reinforcement learning algorithm is to pre-train the policy to imitate an expert using a small set of demonstrations from said expert. However, simply using supervised learning to extrapolate from demonstrations is often not sufficient, since the training set usually will not cover enough of the state space. Worse still, unfamiliar observations will tend to result in actions that push the agent farther away from regions of state space covered by the training data. While fine-tuning with an RL algorithm can correct these issues, the result is still inefficient in the number of samples required.

The thesis proposes to robustify the pre-training process by the use of variational dropout. The addition of a regularisation term based on the variational inference literature is known to sparsify neural networks while minimising degradation in performance. Results show that using this term to regularise the supervised pre-training step results in initial policies that perform better as well as learn faster during RL fine-tuning. Additionally, this regularisation results in a network architecture with learned Gaussian uncertainty on the network weights. This can be used to estimate model uncertainty from samples.


\subsection{Sampling-Based Planning for RL}
Although learning from demonstrations can be effective for speeding up reinforcement learning, it relies on the availability of suitable training data. Such data is often derived from human demonstrations, especially in the case of unknown dynamics where classical methods can't be used. However, human demonstrations are costly to acquire for many problems, and supervised learning generally requires large datasets to work. This thesis proposes a method to automatically generate demonstrations for pre-training using rapidly-exploring random trees, a class of sampling-based algorithms from the planning literature. RRTs are capable of exploring the state space more efficiently than untrained RL agents in the face of a sparse reward signal. However, as more data is acquired, eventually the RL agent will overtake the RRT in terms of the time taken to find a single successful trajectory. Using RRTs to find initial demonstrations, and then refining those demonstrations into a policy using RL, achieves the best of both worlds.

This combination of sampling-based planning and reinforcement learning greatly reduces the overall number of environment interactions needed to learn an optimal policy, and can solve problems that are infeasible even for state-of-the-art RL algorithms. Further, we prove theoretical guarantees and bounds that are not available in standard reinforcement learning with continuous control. Finally, the bridging of planning and RL opens the door to applying many optimisations from the planning literature to RL problems.


\subsection{Directing Exploration with Bayesian Methods}
One of the major causes for inefficient exploration in RL is that an agent will often revisit the neighbourhood of a previously explored state, gaining little to no additional information that can help improve the policy. This occurs even when there are no rewards pulling the agent towards such regions. By default, RL agents on continuous actions and states do not maintain an explicit memory of what they have experienced, and only rewards can influence them to seek out or avoid a region of state space. The redundancy in exploration is especially severe in problems where there is a bottleneck in state space, a phenomenon which often occurs when seeking to manipulate physical objects.

A method is presented to discourage revisitation of previously explored regions based on Bayesian linear regression and model uncertainty. Unlike previous Bayesian exploration algorithms, this method can handle both high-dimensional observations (such as images), continuous actions, and large amounts of data. Further, it is highly flexible and can augment any standard RL algorithm. Experiments verify that the method explores more of the state space than state-of-the-art algorithms. It also compares favourably to other exploration-encouraging techniques on a range of simulated and real control problems. Finally, convergence time and final performance are greatly improved, particularly on those problems where the state space has tight bottlenecks.

\section{Outline}
This first chapter discussed the increasing robotic automation of human labour, which drives the need for autonomous agents that can carry out complex tasks with little to no human supervision. Reinforcement learning was introduced as a family of algorithms for learning how to control such agents without knowledge of system dynamics. This thesis builds on the reinforcement learning literature and addresses some of its weaknesses in order to make RL algorithms more practical and efficient.

\Chapref{background} explains the theoretical background that underpins the contributions of this thesis. It begins with introducing supervised learning for regression in~\secref{02:regression}, then proceeds to explain the fundamentals of reinforcement learning in~\secref{02:rl}.~\secref{02:exploration} describes the challenges of exploration in RL, particularly the trade-off between exploration and exploitation. Finally,~\secref{02:lfd} discusses learning from demonstration, an approach for learning policies by mimicking an expert that is known to behave correctly.

\Chapref{variational_dropout} proposes to apply a regularisation method based on model parameter noise and variational inference to learning-from-demonstration algorithms, thus improving their generalisation. This is followed by an RL fine-tuning step. The problem is introduced in~\secref{03:introduction} and~\secref{03:related} discusses related work. The method is then described in~\secref{03:vd} and the theoretical justification behind it is explained. A series of experiments in~\secref{03:results} verifies that the method is able to achieve better sample complexity than a baseline using standard $l_2$ regularisation, and also enjoys greater resilience to noise in the demonstrations and to low volumes of training data.

\Chapref{r3l} introduces an algorithm for generating demonstrations in the absence of an outside expert by using sampling-based planners.~\secref{04:introduction} motivates the approach in the context of sparse rewards and related work is discussed in~\secref{04:related_work}.~\secref{04:rl_random_walk} presents a novel interpretation of sparse-reward RL as a random walk in the space of policy parameters, which explains some of the inefficiency and poor robustness of state-of-the-art RL algorithms in this setting.~\secref{04:r3l} then provides the necessary preliminary knowledge on planning algorithms, and explains how they are applied for demonstration in reinforcement learning problems. Theoretical results are then given in~\secref{04:exploration_guarantees}, which guarantee that the method will find a solution and bound the time complexity of doing so. The proposed algorithm is evaluated in~\secref{04:experiments} on a series of RL benchmarks, and results confirm reduced sample complexity and increased robustness in comparison with both naive RL algorithms and state-of-the-art exploration methods.

In \chapref{bayesian_curiosity} a method is presented for guiding exploration in RL by adding a secondary reward that is intrinsic to the model. This reward is designed to be proportional to the model uncertainty, so that it encourages visitation of novel states, which differ greatly from what has previously been encountered.~\secref{05:introduction} describes the idea behind the method, and related work is laid out in~\secref{05:related_work}. The Bayesian linear regression machinery which underpins this approach is discussed in~\secref{05:blr_recap}. Then,~\secref{05:bayesian_curiosity} describes the algorithm itself, which uses the predictive uncertainty of a BLR with a learned latent space representation to generate an intrinsic reward. This is followed in~\secref{05:experiments} by experiments on both simulated and real-world tasks. Results verify the benefits of the method in terms of learning time and robustness, which are significant even when applied on top of learning-from-demonstration.

Finally, \Chapref{conclusion} concludes the thesis by summarising the contributions in~\secref{06:contributions} and offering directions for future work in~\secref{06:future_work}.



%**********************************  nomenclature  *************************************



% first letter Z is for Acronyms
\nomenclature[z-rl]{$RL$}{Reinforcement Learning}
\nomenclature[z-irl]{$IRL$}{Inverse Reinforcement Learning}
\nomenclature[z-rrt]{$RRT$}{Rapidly-exploring Random Tree}
\nomenclature[z-blr]{$BLR$}{Bayesian Linear Regression}
\nomenclature[z-se]{$SE$}{Squared Error}
\nomenclature[z-sse]{$SSE$}{Sum of Squared Errors}
\nomenclature[z-ls]{$LS$}{Least Squares}
\nomenclature[z-nn]{$NN$}{Neural Network}
\nomenclature[z-cnn]{$CNN$}{Convolutional Neural Network}
\nomenclature[z-dcnn]{$DCNN$}{Deep Convolutional Neural Network}
\nomenclature[z-rhs]{$RHS$}{Right Hand Side}
\nomenclature[z-sgd]{$SGD$}{Stochastic Gradient Descent}
\nomenclature[z-rgb]{$RGB(D)$}{Red, Green, Blue(, Depth)}
\nomenclature[z-lml]{$LML$}{Log Marginal Likelihood}
\nomenclature[z-mse]{$MSE$}{Mean Squared Error}
\nomenclature[z-mdp]{$MDP$}{Markov Decision Process}
\nomenclature[z-pg]{$PG$}{Policy Gradient}
\nomenclature[z-trpo]{$TRPO$}{Trust Region Policy Optimisation}
\nomenclature[z-kl]{$KL$}{Kullback-Leibler (divergence)}
\nomenclature[z-lfd]{$LfD$}{Learning from Demonstration(s)}
\nomenclature[z-qlfd]{$QLfD$}{Q-Learning from Demonstration(s)}
\nomenclature[z-pofd]{$POfD$}{Policy Optimisation from Demonstration(s)}
\nomenclature[z-vi]{$VI$}{Variational Inference}
\nomenclature[z-vd]{$VD$}{Variational Dropout}
\nomenclature[z-elbo]{$ELBO$}{Evidence Lower BOund}
\nomenclature[z-bc]{$BC$}{Behaviour(al) Cloning}
\nomenclature[z-fem]{$FEM$}{Feature Expectation Matching}
\nomenclature[z-gail]{$GAIL$}{Generative Adversarial Imitation Learning}
\nomenclature[z-sd]{$SD$}{Standard Deviation}
\nomenclature[z-dof]{$DOF$}{Degree Of Freedom}
\nomenclature[z-ik]{$IK$}{Inverse Kinematics}
\nomenclature[z-ou]{$OU$}{Ornstein-Uhlenbeck process}
\nomenclature[z-r3l]{$R3L$}{Rapidly-exploring Random Reinforcement Learning}
\nomenclature[z-prm]{$PRM$}{Probabilistic RoadMap}
\nomenclature[z-pc]{$PC$}{Probabilistically Complete/Probabilistic Completeness}
\nomenclature[z-ddpg]{$DDPG$}{Deep Deterministic Policy Gradient}
\nomenclature[z-vime]{$VIME$}{Variational Information Maximising Exploration}
\nomenclature[z-vae]{$VAE$}{Variational Auto-Encoder}
\nomenclature[z-gan]{$GAN$}{Generative Adversarial Network}
\nomenclature[z-ucb]{$UCB$}{Upper Confidence Bound}
\nomenclature[z-sarsa]{$SARSA$}{State-Action-Reward-State-Action}
\nomenclature[z-smw]{$SMW$}{Sherman-Morrison-Woodbury}
\nomenclature[z-nll]{$NLL$}{Negative Log-Likelihood}
\nomenclature[z-rnd]{$RND$}{Random Network Distillation}
\nomenclature[z-gp]{$GP$}{Gaussian Process}
\nomenclature[z-relu]{$ReLU/relu$}{Rectified Linear Unit}





% first letter A is for Roman symbols                                 
\nomenclature[a-L]{$\mathcal{L}$}{loss function}
\nomenclature[a-E]{$E$}{error function}
\nomenclature[a-kl]{$D_{KL}(\cdot\parallel\cdot)$}{KL divergence}
\nomenclature[a-norm]{$\mathcal{N}$}{normal distribution}
\nomenclature[a-a]{$a$}{action}
\nomenclature[a-s]{$s$}{state}
\nomenclature[a-S]{$\mathcal{S}$}{state space}
\nomenclature[a-A]{$\mathcal{A}$}{action space}
\nomenclature[a-o]{$o$}{observation}
\nomenclature[a-T]{$T$}{transition dynamics}
\nomenclature[a-R]{$R$}{reward function}
\nomenclature[a-r]{$r$}{reward}
\nomenclature[a-H]{$H$}{time horizon}
\nomenclature[a-J]{$J_\pi$}{expected discounted return for policy $\pi$}
\nomenclature[a-V]{$V(\cdot)$}{state value function}
\nomenclature[a-Q]{$Q(\cdot)$}{state-action value function}
\nomenclature[a-adv]{$A(\cdot)$}{advantage function}
\nomenclature[a-unif]{$\mathcal{U}$}{uniform distribution}
\nomenclature[a-qphi]{$q_\phi$}{parameterised proposal distribution}
\nomenclature[a-expected]{$\mathbb{E}$}{expected value}
\nomenclature[a-tree]{$\mathbb{T}$}{tree data structure}
\nomenclature[a-dynamics]{$f(\cdot,\cdot)$}{continuous dynamics}
\nomenclature[a-F]{$\mathcal{F}$}{valid state space}
\nomenclature[a-Fg]{$\mathcal{F}_{goal}$}{goal space}
\nomenclature[a-eye]{$\bm{I}$}{identity matrix}
\nomenclature[a-m0]{$\bm{m}$}{mean vector}
\nomenclature[a-s0]{$\bm{S}$}{covariance matrix}
\nomenclature[a-w]{$\bm{w}$}{weight vector}
\nomenclature[a-x]{$\bm{x}$}{vector input}
\nomenclature[a-X]{$\bm{X}$}{matrix input}
\nomenclature[a-sx]{$x$}{scalar input or generic input}

% first letter G is for Greek Symbols                                                 
\nomenclature[g-p]{$\pi$}{Policy}    
\nomenclature[g-theta]{$\theta/\psi/\phi$}{parameters}    
\nomenclature[g-theta]{$\phi(\cdot)$}{basis/kernel/transformation function}
\nomenclature[g-lambda]{$\lambda$}{regularisation weight}
\nomenclature[g-eps]{$\epsilon$}{some small positive number}
\nomenclature[g-delta]{$\partial\square$}{partial derivative}
\nomenclature[g-nabla]{$\nabla$}{gradient}
\nomenclature[g-delerr]{$\delta^{(i)}$}{gradient of the error at the $i^{th}$ layer}
\nomenclature[g-gamma]{$\gamma$}{discount factor}
\nomenclature[g-mu]{$\mu$}{mean}
\nomenclature[g-sigma]{$\sigma$}{standard deviation}
\nomenclature[g-Theta]{$\Theta$}{parameter space}
\nomenclature[g-Upsilon]{$\Upsilon(\cdot,\cdot)$}{steering function}
\nomenclature[g-tau]{$\tau$}{trajectory}



% first letter X is for Other Symbols                                         
\nomenclature[x-conv]{$\bm{*}$}{convolution operator}
\nomenclature[x-sim]{$\sim$}{distributed according to}
\nomenclature[x-prop]{$\propto$}{proportional to}


% first letter R is for superscripts
\nomenclature[r-layer]{$\square^{(i)}$}{layer index}
\nomenclature[r-opt]{$\square^*$}{optimum}
\nomenclature[r-estimate]{$\widetilde{\square}/\hat{\square}$}{estimator}
\nomenclature[r-]{$\dot{\square}$}{rate of change}


% first letter S is for subscripts
\nomenclature[s-t]{$\square_t$}{timestep $t$}
\nomenclature[s-i]{$\square_i$}{index $i$}
\nomenclature[s-theta]{$\square_\theta$}{expression parameterised by $\theta$}
\nomenclature[s-]{$\norm{\cdot}_p$}{$p$-norm}
\nomenclature[s-p0]{$p_0$}{initial state distribution}
