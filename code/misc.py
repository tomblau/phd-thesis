#RFFBLR uncertainty
import scipy
import theano.tensor as TT
from theano import function
from rllab.policies.components import BayesianLinearRegression as BLR
from rllab.policies.components import IdentityKernel, QMCRFFKernel
from rllab.envs.pendulum_env import PendulumEnv
from rllab.envs.normalized_env import normalize
import numpy as np
import matplotlib.pyplot as plt

seed=0
env = PendulumEnv(seed=seed)
feature_dim = 2
output_dim = 1
num_rff = 100
m = num_rff * feature_dim
kernel = QMCRFFKernel(M=m, d=feature_dim)
blr = BLR(2*kernel.M, 1, 0, 1e-2, 1e2)
x = np.arange(-np.pi, np.pi, 2*np.pi/1000)
y = np.arange(-8, 8, 16/1000)
xy = np.transpose([np.tile(x, len(y)), np.repeat(y, len(x))])
ptp_var = TT.dmatrix("ptp")
updates = blr._p_update_helper(ptp_var)
update_fn = function(inputs=[ptp_var], updates=updates)

#generate data
states = np.zeros((env.horizon,2))
env.reset()
for i in range(env.horizon):
	action = np.random.uniform(-1,1,size=(1,1))
	_,_,_,_ = env.step(action)
	states[i] = env.env.env.state.flatten()

#update BLR
size = xy.shape[0]
ratio = 10
portion = size//ratio
indices = np.random.choice(size, 1000)
phi = blr.add_bias(kernel.transform(states))
ptp = phi.T.dot(phi).eval()
update_fn(ptp)

#compute intrinsic rewards
res = np.zeros((size,1))
for i in range(ratio):
	res[i*portion : (i+1)*portion] = blr.get_moment_vars(
			kernel.transform(xy[i*portion: (i+1)*portion]))[-1].eval()

x = np.stack([np.arange(-np.pi, np.pi, 2*np.pi/1000) for _ in range(1000)])
y = np.stack([np.arange(-8, 8, 16/1000) for _ in range(1000)]).transpose()
res /= np.log(1e2)
np.savez_compressed("intrinsic_rewards.npz", rew=res, obs=states)



#1-D RFFBLR
seed=0
feature_dim = 1
output_dim = 1
num_rff = 100
m = num_rff * feature_dim
kernel = QMCRFFKernel(M=m, d=feature_dim)
blr = BLR(2*kernel.M, 1, 0, 1e-2, 1e2)
x = np.random.uniform(0, 1, (10,1))
y = np.sin(2*np.pi*x)
ptp_var = TT.dmatrix("ptp")
p_var = TT.dmatrix("p")
tar_var = TT.dmatrix("tar")
updates = blr._p_update_helper(ptp_var, p_var, tar_var)
update_fn = function(inputs=[ptp_var, p_var, tar_var], updates=updates)
trans_x = kernel.transform(x).eval()
phi = blr.add_bias(trans_x)
ptp = phi.T.dot(phi)
update_fn(ptp, phi, y)

inp = np.arange(0,1,1e-3).reshape(-1,1)
moments = blr.get_moment_vars(kernel.transform(inp))
mean = moments[0].eval()
var = moments[1].eval()







#polynomial BLR uncertainty
import scipy
import theano.tensor as TT
from theano import function, shared
from rllab.policies.components import BayesianLinearRegression as BLR
from rllab.policies.components import IdentityKernel, QMCRFFKernel
from rllab.envs.normalized_env import normalize
import numpy as np
import matplotlib.pyplot as plt


def ordernpoly(x, n=10):
    return np.stack([x**i for i in range(n+1)]).transpose()

#generate data
seed = 0; np.random.seed(seed)
n = 50; sd = 0.02; order = 10
x_train = np.concatenate([np.random.uniform(0.3,1, n), np.random.uniform(-0.3,-1, n)])
eps = np.random.normal(0,sd, n)
y_train = np.sin(1+x_train**2) + eps
phi_train = ordernpoly(x_train, order)
x_test = np.random.uniform(-0.3,0.3, n)
y_test = np.sin(1+x_test**2) + np.random.normal(0,sd, n)
phi_test = ordernpoly(x_test, order)

feature_dim = order; output_dim = 1; kernel = IdentityKernel()
blr = BLR(feature_dim, output_dim, 0, 1e-6, 2.5e3)
ptp_var = TT.dmatrix("ptp"); p_var = TT.dmatrix("p")
tar_var = TT.dmatrix("tar"); lamd_var = TT.dmatrix("lambda")
p_updates = blr._p_update_helper(ptp_var, p_var, tar_var)
hp_updates = blr._hp_update_helper(p_var, lamd_var, tar_var)
blr.opts['f_p_update_iterative'] = function(inputs=[p_var, ptp_var, tar_var],
											updates=p_updates)
blr.opts['f_hp_update_iterative'] = function(inputs=[p_var, lamd_var, tar_var],
											updates=hp_updates)
p_update_fn = blr.opts['f_p_update_iterative']
hp_update_fn = blr.opts['f_hp_update_iterative']

# update BLR
ptp = phi_train.T.dot(phi_train)
p_update_fn(phi_train, ptp, y_train.reshape(-1,1))

#NLML optimisation
#blr.learn_all_params(phi_train[:,1:], y_train.reshape(-1,1))
m_test = blr.get_moment_vars(shared(phi_test[:,1:]))[0].eval()
sd_test = blr.get_moment_vars(shared(phi_test[:,1:]))[1].eval()
print(((m_test - y_test)**2).mean())


x_plot = np.arange(-np.pi,np.pi,1/1000)
y_plot = np.sin(1+x_plot**2)
phi_plot = kernel.transform(x_plot, order)
m_plot,sd_plot, _ = blr.get_moment_vars(shared(phi_plot[:,1:]))
m_plot = m_plot.eval(); sd_plot = np.sqrt(sd_plot.eval())
plt.plot(x_plot, y_plot)
plt.plot(x_plot, m_plot)
plt.fill_between(x_plot, (m_plot+sd_plot).reshape(-1), (m_plot-sd_plot).reshape(-1), alpha=0.5)
#plt.fill_between(x_plot, (m_plot+2*sd_plot).reshape(-1), (m_plot-2*sd_plot).reshape(-1), alpha=0.25)
plt.scatter(x_train,y_train)
plt.scatter(x_test, y_test)
plt.xlim([-1,1]); plt.ylim([0.8,1.1]); plt.show()


def mnll(actual_mean, pred_mean, pred_var):
	"""
	Mean Negative Log Likelihood
	:param actual_mean:
	:param pred_mean:
	:param pred_var:
	"""
	log_part = np.log(2 * math.pi * pred_var) / 2.0
	unc_part = (pred_mean - actual_mean)**2 / (2 * pred_var)
	summed_parts = log_part + unc_part
	return summed_parts.mean()

mnll(y_test, m_test, sd_test)

#polynomial BLR uncertainty with NLML maximisation


import scipy
import theano.tensor as TT
from theano import function, shared
from rllab.policies.components import BayesianLinearRegression as BLR
from rllab.policies.components import IdentityKernel, QMCRFFKernel
from rllab.envs.normalized_env import normalize
import numpy as np
import matplotlib.pyplot as plt



#generate data
seed = 1; np.random.seed(seed); feature_dim = 1; output_dim = 1; num_rff = 100
n_train = 4; n_test = 50; sd = 0.1; order = 10
m = num_rff * feature_dim
kernel = QMCRFFKernel(M=m, d=feature_dim)
blr = BLR(2*m, output_dim, 0, 1e-6, 1/sd**2)
x_train = np.array([0.,0.4,0.6,1.0]).reshape(-1,1)
eps = np.random.normal(0,sd, (n_train,1))
y_train = np.sin(2*np.pi*x_train) + eps
phi_train = blr.add_bias(kernel.transform(x_train).eval())
x_test = np.random.uniform(0,1, n_test)
y_test = np.sin(2*np.pi*x_test) + np.random.normal(0,sd, n_test)
phi_test = blr.add_bias(kernel.transform(x_test.reshape(-1,1)).eval())


ptp_var = TT.dmatrix("ptp"); p_var = TT.dmatrix("p")
tar_var = TT.dmatrix("tar"); lamd_var = TT.dmatrix("lambda")
p_updates = blr._p_update_helper(ptp_var, p_var, tar_var)
hp_updates = blr._hp_update_helper(p_var, lamd_var, tar_var)
blr.opts['f_p_update_iterative'] = function(inputs=[p_var, ptp_var, tar_var],
											updates=p_updates)
blr.opts['f_hp_update_iterative'] = function(inputs=[p_var, lamd_var, tar_var],
											updates=hp_updates)
p_update_fn = blr.opts['f_p_update_iterative']
hp_update_fn = blr.opts['f_hp_update_iterative']

# update BLR
ptp = phi_train.T.dot(phi_train)
p_update_fn(phi_train, ptp, y_train.reshape(-1,1))

#NLML optimisation
#blr.learn_all_params(phi_train[:,1:], y_train.reshape(-1,1))
m_test = blr.get_moment_vars(shared(phi_test[:,1:]))[0].eval()
sd_test = blr.get_moment_vars(shared(phi_test[:,1:]))[1].eval()
print(((m_test - y_test)**2).mean())


x_plot = np.arange(-np.pi,np.pi,1/1000)
phi_plot = blr.add_bias(kernel.transform(x_plot.reshape(-1,1)).eval())
y_plot = np.sin(2*np.pi*x_plot)
m_plot,sd_plot, _ = blr.get_moment_vars(shared(phi_plot[:,1:]))
m_plot = m_plot.eval(); sd_plot = np.sqrt(sd_plot.eval())
plt.plot(x_plot, y_plot)
plt.plot(x_plot, m_plot)
plt.fill_between(x_plot, (m_plot+sd_plot).reshape(-1), (m_plot-sd_plot).reshape(-1), alpha=0.5)
#plt.fill_between(x_plot, (m_plot+2*sd_plot).reshape(-1), (m_plot-2*sd_plot).reshape(-1), alpha=0.25)
plt.scatter(x_train,y_train)
plt.xlim([-0.1,1.1]); plt.ylim([-2,2]); plt.show()
