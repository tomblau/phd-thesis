import numpy as np
from testing.test_bandit_algorithms import BernoulliArm, test_algorithm
from bandit_algorithms.epsilon_greedy.epsilon_greedy_algorithm import EpsilonGreedy
import seaborn as sns
import matplotlib.pyplot as plt

colors = ['#396CB1', '#DA7C30', '#3E9651', '#CC2529', '#6B4C9A', '#948B3D', '#ffe119']
plt.style.use('seaborn-white')

plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.serif'] = 'Ubuntu'
plt.rcParams['font.monospace'] = 'Ubuntu Mono'
#plt.rcParams['font.size'] = 12
plt.rcParams['axes.labelsize'] = 12
# plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['axes.titlesize'] = 12
plt.rcParams['xtick.labelsize'] = 12
plt.rcParams['ytick.labelsize'] = 12
plt.rcParams['legend.fontsize'] = 12.5
plt.rcParams['figure.titlesize'] = 14
# plt.rcParams['figure.titleweight'] = 'bold'

fig_dpi = 400
width, height = plt.figaspect(0.68) / 1
# line sizes
dash_width = 1.3
lines_width = 0.7
alpha_iq = 0.20
vert_dash_width = 0.8




###################
##### Bandits #####
###################

#get data
np.random.seed(1)
# Average reward by arm
means = [0.1, 0.1, 0.1, 0.1, 0.9]
n_arms = len(means)
# Shuffle the arms
np.random.shuffle(means)
# Each arm will follow and Bernoulli distribution
arms = list(map(lambda mu: BernoulliArm(mu), means))
# Get the index of the best arm to test if algorithm will be able to learn that
best_arm_index = np.argmax(means)
# Define epsilon value to check the performance of the algorithm using each one
epsilons = [0, 0.01, 0.05, 0.1, 0.2, 0.5]
lbls = [r"$\varepsilon = %s$" %str(eps) for eps in epsilons]
results = [test_algorithm(EpsilonGreedy(eps), arms, num_simulations=5000, horizon=1000) for eps in epsilons]


# Set an aspect ratio
fig_dpi = 400
width, height = plt.figaspect(0.68) / 1
fig = plt.figure(figsize=(width,height), dpi=fig_dpi)
fig.clear()
ax = fig.add_subplot(1,1,1, xlim=[0,1000], ylim=[0,1])
for i in range(len(results)):
    ax.plot(np.arange(results[i][1].shape[0]), results[i][1], label=lbls[i],
        linewidth=0.75, color=colors[i])

plt.grid(True)
ax.set_xlabel("Timesteps")
ax.set_ylabel("Mean Reward")
ax.set_title("5-Armed Bandit")
handles, labels = ax.get_legend_handles_labels()
lgd = ax.legend(handles, labels, fontsize='large', loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=3)
fig.tight_layout(rect=(0,0.01,1,1))
plt.setp(lgd.get_lines(), linewidth=4)
#fig.show()
fig.savefig("/home/tom/phd-thesis-template-master/Chapter2/Figs/bandits.pdf")




###################
##### Cartpole #####
###################

experiments = [
                "experiment_2019_08_20_15_46_36_0001",
                "experiment_2019_08_20_17_05_30_0001",
                "experiment_2019_08_20_13_52_52_0001",
                "experiment_2019_08_20_13_03_37_0001",
                "experiment_2019_08_20_14_04_45_0001",
                "experiment_2019_08_20_14_43_35_0001",
                ]

series = []; smoothed = []
lbls = [r"$\sigma = 0$", r"$\sigma = 0.05$", r"$\sigma = 0.1$",
        r"$\sigma = 0.5$", r"$\sigma = 1.0$", r"$\sigma = 5.0$",]
for exp in experiments:
    data = np.load("%s/series.npz" %exp)
    means = data['means']
    series.append(np.concatenate([means, means[-1:]]))


fig_dpi = 400
width, height = plt.figaspect(0.68) / 1
fig = plt.figure(figsize=(width,height), dpi=fig_dpi)
fig.clear()
ax = fig.add_subplot(1,1,1, xlim=[0,100], ylim=[-10,110])
for i in range(len(series)):
    ax.plot(np.arange(series[i].shape[0]), series[i], label=lbls[i],
        linewidth=1, color=colors[i])

plt.grid(True)
ax.set_xlabel("Timesteps")
ax.set_ylabel("Mean Reward")
ax.set_title("Cartpole")
handles, labels = ax.get_legend_handles_labels()
lgd = ax.legend(handles, labels, fontsize='large', loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=3)
fig.tight_layout(rect=(0,0.01,1,1))
plt.setp(lgd.get_lines(), linewidth=4)
#fig.show()
fig.savefig("/home/tom/phd-thesis-template-master/Chapter2/Figs/cartpole.pdf")




##########################
##### Reward Surface #####
##########################

import numpy as np
import matplotlib.pyplot as plt

w2, h2 = plt.figaspect(0.68**2) / 1.5
x = np.stack([np.arange(-np.pi, np.pi, 2*np.pi/1000) for _ in range(1000)])
y = np.stack([np.arange(-8, 8, 16/1000) for _ in range(1000)]).transpose()
z_dense = np.cos(x)
z_sparse = np.cos(x) > 0.99
intr_data = np.load("intrinsic_rewards.npz")
z_intr = intr_data['rew']; z_intr[z_intr.argmin()] = -1.
obs_intr = intr_data['obs']
z_combined = z_intr.reshape(z_sparse.shape) + z_sparse

fig = plt.figure(figsize=(w2,h2), dpi=fig_dpi); fig.clear()
#dense rewards
ax = fig.add_subplot(1, 2, 1, xlim=[-np.pi, np.pi], ylim=[-8, 8])
cs_dense = ax.contourf(x, y, z_dense.reshape(x.shape), 100, cmap="viridis")
for c in cs_dense.collections: c.set_edgecolor("face")

ax.hlines(y=[-1,1,-1,1], xmin=[-np.pi, -np.pi, np.pi-1, np.pi-1],
    xmax=[1-np.pi, 1-np.pi, np.pi, np.pi], linestyle='--', linewidth=1,
    color="#FF0000")
ax.vlines(x=[1-np.pi,np.pi-1], ymin=[-1,-1], ymax=[1,1], linestyle='--', linewidth=1,
    color="#FF0000")
ax.set_xlabel(r"$\theta$ (rad)"); ax.set_ylabel(r"$\omega$ (rad/s)")
ax.set_title("Dense Rewards")
#sparse rewards
ax = fig.add_subplot(1, 2, 2, xlim=[-np.pi, np.pi], ylim=[-8, 8])
cs_sparse = plt.contourf(x, y, z_sparse.reshape(x.shape), 100, cmap="viridis")
for c in cs_sparse.collections: c.set_edgecolor("face")

ax.hlines(y=[-1,1,-1,1], xmin=[-np.pi, -np.pi, np.pi-1, np.pi-1],
    xmax=[1-np.pi, 1-np.pi, np.pi, np.pi], linestyle='--', linewidth=1,
    color="#FF0000")
ax.vlines(x=[1-np.pi,np.pi-1], ymin=[-1,-1], ymax=[1,1], linestyle='--', linewidth=1,
    color="#FF0000")
ax.set_xlabel(r"$\theta$ (rad)")
ax.set_title("Sparse Rewards")
fig.tight_layout(rect=(-0.03,-0.2,1.02,1.))
cb = fig.colorbar(cs_sparse,ax=fig.axes, shrink=0.9, orientation='horizontal',
    aspect=50, anchor=(0.5, 0.8), ticks=np.arange(0, 1.1, 0.1))
fig.savefig("/home/tom/phd-thesis-template-master/Chapter2/Figs/extrinsic_rewards.pdf")

fig = plt.figure(figsize=(w2,h2), dpi=fig_dpi); fig.clear()
#intrinsic rewards
ax = fig.add_subplot(1, 2, 1, xlim=[-np.pi, np.pi], ylim=[-8, 8])
cs_intr = plt.contourf(x, y, z_intr.reshape(x.shape), 100, cmap="viridis", vmax=z_combined.max())
for c in cs_intr.collections: c.set_edgecolor("face")

ax.set_xlabel(r"$\theta$ (rad)"); ax.set_ylabel(r"$\omega$ (rad/s)")
ax.set_title("Intrinsic Rewards")
plt.scatter(obs_intr[:,0], obs_intr[:,1], s=1, c="#FF0000")
#combined rewards
ax = fig.add_subplot(1, 2, 2, xlim=[-np.pi, np.pi], ylim=[-8, 8])
cs_combined = plt.contourf(x, y, z_combined.reshape(x.shape), 100, cmap="viridis", vmax=z_combined.max())
for c in cs_combined.collections: c.set_edgecolor("face")

ax.set_xlabel(r"$\theta$ (rad)")
ax.set_title("Combined Rewards")
fig.tight_layout(rect=(-0.03,-0.2,1.02,1.))
cb = fig.colorbar(cs_combined,ax=fig.axes, shrink=0.9, orientation='horizontal',
    aspect=50, anchor=(0.5, 0.8), ticks=np.arange(-1.6, 2.3, 0.3))
fig.savefig("/home/tom/phd-thesis-template-master/Chapter2/Figs/intrinsic_rewards.pdf")




#############################
##### Linear Regression #####
#############################s

import matplotlib.pyplot as plt
import numpy as np

seed = 0; np.random.seed(seed)
n = 100; sd = 0.02
x = np.random.uniform(0,1, n)
eps = np.random.normal(0,sd, n)
y = np.sin(1+x**2) + eps
x_plot = np.arange(0,1,1e-3)
y_plot = np.sin(1+x_plot**2)
xp = np.stack([x, np.ones_like(x)]).transpose()
w, linear_r,_, _ = np.linalg.lstsq(xp,y)
a, b = w
model_y = a*x_plot + b

fig_dpi = 400; width, height = plt.figaspect(0.68) / 1
fig = plt.figure(figsize=(width,height), dpi=fig_dpi); fig.clear()
ax = fig.add_subplot(1,1,1, xlim=[0,1], ylim=[0.8,1.1])
ax.plot(x_plot, y_plot, color=colors[0])
ax.fill_between(x_plot, y_plot + sd, y_plot - sd, color=colors[0], alpha=0.25, linewidth=0)
ax.plot(x_plot, model_y, color=colors[1])
ax.scatter(x,y, s=1, c="#000000")
ax.set_xlabel("x"); ax.set_ylabel("y"); ax.set_title("Linear basis")
fig.savefig("/home/tom/phd-thesis-template-master/Chapter2/Figs/linear_regression.pdf")

def order10poly(x):
    return np.stack([x**i for i in range(11)]).transpose()

phi = order10poly(x)
w, poly_r,_, _ = np.linalg.lstsq(phi,y)
plot_phi = order10poly(x_plot)
model_y = plot_phi.dot(w)

fig_dpi = 400; width, height = plt.figaspect(0.68) / 1
fig = plt.figure(figsize=(width,height), dpi=fig_dpi); fig.clear()
ax = fig.add_subplot(1,1,1, xlim=[0,1], ylim=[0.8,1.1])
ax.plot(x_plot, y_plot, color=colors[0])
ax.fill_between(x_plot, y_plot + sd, y_plot - sd, color=colors[0], alpha=0.25, linewidth=0)
ax.scatter(x,y, s=1, c="#000000")
ax.plot(x_plot, model_y, color=colors[1])
ax.set_xlabel("x"); ax.set_ylabel("y"); ax.set_title("Polynomial basis")
fig.savefig("/home/tom/phd-thesis-template-master/Chapter2/Figs/poly_regression.pdf")




#################################
##### Linear Regularisation #####
#################################

import matplotlib.pyplot as plt
import numpy as np
import scipy as sp

def ordernpoly(x, n=10):
    return np.stack([x**i for i in range(n+1)]).transpose()

seed = 0; np.random.seed(seed)
n = 50; sd = 0.02
x_train = np.concatenate([np.random.uniform(0.3,1, n), np.random.uniform(-0.3,-1, n)])
x_test = np.random.uniform(-1,0, n)
eps = np.random.normal(0,sd, 2*n)
y_train = np.sin(1+x_train**2) + eps
phi_train = ordernpoly(x_train, order)
x_test = np.random.uniform(-0.3,0.3, n)
y_test = np.sin(1+x_test**2) + np.random.normal(0,sd, n)
phi_test = ordernpoly(x_test, order)
x_plot = np.arange(-1,1,1e-3)
y_plot = np.sin(1+x_plot**2)

w, poly_r,_, _ = np.linalg.lstsq(phi_train,y_train)
phi_plot = ordernpoly(x_plot)
y_model = phi_plot.dot(w)

fig_dpi = 400; width, height = plt.figaspect(0.68) / 1
fig = plt.figure(figsize=(width,height), dpi=fig_dpi); fig.clear()
ax = fig.add_subplot(1,1,1, xlim=[-1,1], ylim=[0.8,1.1])
ax.plot(x_plot, y_plot, color=colors[0])
ax.fill_between(x_plot, y_plot + sd, y_plot - sd, color=colors[0], alpha=0.25, linewidth=0)
ax.scatter(x_train,y_train, s=1, c="#000000")
ax.scatter(x_test,y_test, s=1, c="#FF0000")
ax.plot(x_plot, y_model, color=colors[1])
ax.set_xlabel("x"); ax.set_ylabel("y"); ax.set_title("Unregularised")
fig.savefig("/home/tom/phd-thesis-template-master/Chapter2/Figs/unregularised.pdf")

w, reason, i, poly_r,_, _, _, _, _, _ = sp.sparse.linalg.lsqr(phi_train,y_train, damp=0.007)
y_model = phi_plot.dot(w)
phi_test = ordernpoly(x_test)
print(((phi_test.dot(w) - y_test)**2).mean())
print(y_test.var())

fig_dpi = 400; width, height = plt.figaspect(0.68) / 1
fig = plt.figure(figsize=(width,height), dpi=fig_dpi); fig.clear()
ax = fig.add_subplot(1,1,1, xlim=[-1,1], ylim=[0.8,1.1])
ax.plot(x_plot, y_plot, color=colors[0])
ax.fill_between(x_plot, y_plot + sd, y_plot - sd, color=colors[0], alpha=0.25, linewidth=0)
ax.scatter(x_train,y_train, s=1, c="#000000")
ax.scatter(x_test,y_test, s=1, c="#FF0000")
ax.plot(x_plot, y_model, color=colors[1])
ax.set_xlabel("x"); ax.set_ylabel("y"); ax.set_title(r"$l_2$ Regularisation")
fig.savefig("/home/tom/phd-thesis-template-master/Chapter2/Figs/l2_regularisation.pdf")




###############
##### BLR #####
###############

import theano.tensor as TT
from theano import function, shared
from rllab.policies.components import BayesianLinearRegression as BLR
from rllab.policies.components import IdentityKernel
import numpy as np
import matplotlib.pyplot as plt

def ordernpoly(x, n=10):
    return np.stack([x**i for i in range(n+1)]).transpose()

regularised = True
#generate data
seed = 0; np.random.seed(seed)
n = 50; sd = 0.02; order = 10
x_train = np.concatenate([np.random.uniform(0.3,1, n), np.random.uniform(-0.3,-1, n)])
y_train = np.sin(1+x_train**2) + np.random.normal(0,sd, 2*n)
phi_train = ordernpoly(x_train, order)
x_test = np.random.uniform(-0.3,0.3, n)
y_test = np.sin(1+x_test**2) + np.random.normal(0,sd, n)
phi_test = ordernpoly(x_test, order)

feature_dim = order; output_dim = 1; kernel = IdentityKernel()
blr = BLR(feature_dim, output_dim, 0, 1e-6, 2.5e3)
ptp_var = TT.dmatrix("ptp"); p_var = TT.dmatrix("p")
tar_var = TT.dmatrix("tar"); lamd_var = TT.dmatrix("lambda")
p_updates = blr._p_update_helper(ptp_var, p_var, tar_var)
hp_updates = blr._hp_update_helper(p_var, lamd_var, tar_var)
blr.opts['f_p_update_iterative'] = function(inputs=[p_var, ptp_var, tar_var],
                                            updates=p_updates)
blr.opts['f_hp_update_iterative'] = function(inputs=[p_var, lamd_var, tar_var],
                                            updates=hp_updates)
p_update_fn = blr.opts['f_p_update_iterative']
hp_update_fn = blr.opts['f_hp_update_iterative']

# update BLR
ptp = phi_train.T.dot(phi_train)
p_update_fn(phi_train, ptp, y_train.reshape(-1,1))
if regularised:
    blr.learn_all_params(phi_train[:,1:], y_train.reshape(-1,1))

x_plot = np.arange(-np.pi,np.pi,1/1000)
y_plot = np.sin(1+x_plot**2); phi_plot = ordernpoly(x_plot, order)
m_plot,sd_plot, _ = blr.get_moment_vars(shared(phi_plot[:,1:]))
m_plot = m_plot.eval().reshape(-1); sd_plot = np.sqrt(sd_plot.eval()).reshape(-1)

fig_dpi = 400; width, height = plt.figaspect(0.68) / 1
fig = plt.figure(figsize=(width,height), dpi=fig_dpi); fig.clear()
ax = fig.add_subplot(1,1,1, xlim=[-1,1], ylim=[0.75,1.05])
ax.plot(x_plot, m_plot, color=colors[1], zorder=1)
ax.fill_between(x_plot, m_plot+sd_plot, m_plot-sd_plot, alpha=0.3, linewidth=0, color=colors[1])
ax.fill_between(x_plot, m_plot+2*sd_plot, m_plot-2*sd_plot, alpha=0.15, linewidth=0, color=colors[1])
ax.plot(x_plot, y_plot, color=colors[0], zorder=0)
ax.scatter(x_train,y_train, s=1, c="#000000"); ax.scatter(x_test,y_test, s=1, c="#FF0000")
ax.set_xlabel("x"); ax.set_ylabel("y");
ax.set_title("LML-BLR" if regularised else "BLR")
filename = "nlml_blr" if regularised else "blr"
fig.tight_layout(rect=(-0.02,-0.02,1.02,1.02))
plt.grid(True)
fig.savefig("/home/tom/phd-thesis-template-master/Chapter2/Figs/%s.pdf" %filename)




###########################
##### BLR Uncertainty #####
###########################

import scipy
import theano.tensor as TT
from theano import function, shared
from rllab.policies.components import BayesianLinearRegression as BLR
from rllab.policies.components import IdentityKernel, QMCRFFKernel
from rllab.envs.normalized_env import normalize
import numpy as np
import matplotlib.pyplot as plt



#generate data
seed = 1; np.random.seed(seed); feature_dim = 1; output_dim = 1; num_rff = 100
n_train = 4; n_test = 50; sd = 0.1; order = 10
m = num_rff * feature_dim
kernel = QMCRFFKernel(M=m, d=feature_dim)
blr = BLR(2*m, output_dim, 0, 1e-6, 1/sd**2)
x_train = np.array([0.,0.4,0.6,1.0]).reshape(-1,1)
eps = np.random.normal(0,sd, (n_train,1))
y_train = np.sin(2*np.pi*x_train) + eps
phi_train = blr.add_bias(kernel.transform(x_train).eval())
x_test = np.random.uniform(0,1, n_test)
y_test = np.sin(2*np.pi*x_test) + np.random.normal(0,sd, n_test)
phi_test = blr.add_bias(kernel.transform(x_test.reshape(-1,1)).eval())

ptp_var = TT.dmatrix("ptp"); p_var = TT.dmatrix("p")
tar_var = TT.dmatrix("tar"); lamd_var = TT.dmatrix("lambda")
p_updates = blr._p_update_helper(ptp_var, p_var, tar_var)
hp_updates = blr._hp_update_helper(p_var, lamd_var, tar_var)
blr.opts['f_p_update_iterative'] = function(inputs=[p_var, ptp_var, tar_var],
                                            updates=p_updates)
blr.opts['f_hp_update_iterative'] = function(inputs=[p_var, lamd_var, tar_var],
                                            updates=hp_updates)
p_update_fn = blr.opts['f_p_update_iterative']
hp_update_fn = blr.opts['f_hp_update_iterative']

# update BLR
ptp = phi_train.T.dot(phi_train)
p_update_fn(phi_train, ptp, y_train.reshape(-1,1))

x_plot = np.arange(-np.pi,np.pi,1/1000)
phi_plot = blr.add_bias(kernel.transform(x_plot.reshape(-1,1)).eval())
y_plot = np.sin(2*np.pi*x_plot)
m_plot,sd_plot, _ = blr.get_moment_vars(shared(phi_plot[:,1:]))
m_plot = m_plot.eval(); sd_plot = np.sqrt(sd_plot.eval())

fig_dpi = 400; width, height = plt.figaspect(0.68) / 1
fig = plt.figure(figsize=(width,height), dpi=fig_dpi); fig.clear()
ax = fig.add_subplot(1,1,1, xlim=[-0.1,1.1], ylim=[-2,2])
ax.plot(x_plot, y_plot, color=colors[0])
ax.plot(x_plot, m_plot, color=colors[1])
ax.fill_between(x_plot, (m_plot+sd_plot).reshape(-1), (m_plot-sd_plot).reshape(-1), color=colors[1], alpha=0.25, linewidth=0)
ax.scatter(x_train,y_train, s=40, facecolors='none', edgecolors="#000000",zorder=10)
ax.set_xlabel("x"); ax.set_ylabel("y"); ax.set_title(r"BLR Uncertainty")
fig.tight_layout(rect=(-0.02,-0.02,1.0,1.02))
plt.grid(True)
fig.savefig("/home/tom/phd-thesis-template-master/Chapter5/Figs/blr_uncertainty.pdf")
