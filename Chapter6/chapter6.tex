%!TEX root = ../thesis.tex
%*******************************************************************************
%****************************** Third Chapter **********************************
%*******************************************************************************
\chapter{Conclusion}
\label{chap:conclusion}

% **************************** Define Graphics Path **************************
\ifpdf
    \graphicspath{{Chapter6/Figs/Raster/}{Chapter6/Figs/PDF/}{Chapter6/Figs/}}
\else
    \graphicspath{{Chapter6/Figs/Vector/}{Chapter6/Figs/}}
\fi

This thesis addressed the problem of learning how to solve sequential decision problems, such as robotic grasping and locomotion, from experience. Problems of this kind are typically solved by applying the reinforcement learning framework, in which a control policy is gradually learned by interacting with the environment and observing the outcomes of actions and a reward signal. The most significant drawback of the reinforcement learning approach is that it has high sample complexity, and requires many interactions between an agent and an environment in order to learn a policy that can perform the task reliably and efficiently.

To ameliorate the sample complexity of RL algorithms, this work takes a holistic approach and proposes improvements at all stages of the learning process. First, demonstration trajectories that can successfully complete the task are generated by searching the state space with expansive search trees. Then, such trajectories are distilled into initial policies by supervised learning with variational regularisation to improve robustness. Finally, the policies are refined by reinforcement learning, where the reinforcement signal is augmented to reward discovering states with high uncertainty.

This chapter concludes the thesis, reviewing the contributions in~\secref{06:contributions} and proposing directions for future work in~\secref{06:future_work}.



\section{Summary of Contributions}
\label{sec:06:contributions}



Several contributions were made in this thesis in the areas of learning from demonstration and exploration in the RL setting. This was achieved by combining reinforcement learning algorithms with methods from other fields: variational inference, sampling-based planning, and Baeysian methods. The following section briefly recapitulates each one.



\subsection{Behaviour Cloning with Variational Dropout}



Behaviour cloning is a popular approach for learning control policies by mimicking the observed actions of an expert. The primary shortcoming of such methods is that there is typically a discrepancy between the states that have been demonstrated by the expert and the states that a cloned policy will encounter in practice.~\chapref{variational_dropout} proposed to regularise behaviour cloning by placing a Gaussian prior on model parameters and tuning the variance of this Gaussian using variational inference. Parameters whose associated variance exceeds a certain threshold are pruned from the model.

The variational dropout regularisation improves the generalisation of the cloned policies to novel states, resulting in higher performance and faster learning during RL fine-tuning. Furthermore, this regularisation is more robust, both to the choice of noise in the expert and to the volume of training data, when compared with standard $l_2$ regularisation.



\subsection{Generating Demonstrations by Planning}



~\chapref{r3l} introduced a novel view of reinforcement learning with sparse rewards, interpreting it as a random walk in policy parameter space. This interpretation explains in part why sparse reward RL tends to exhibit high variance between different instantiations of the same algorithm, and predicts that policy gradient methods may not find a solution even in the limit of infinite interactions.

Behaviour cloning can help mitigate these shortcomings, but is predicated on the availability of expert demonstrations. Such demonstrations may be difficult or costly to acquire, potentially requiring either individual human demonstrators or knowledge of the physical dynamics of the system. Concordantly,~\chapref{r3l} proposes Rapidly Randomly-exploring Reinforcement Learning, a method for automatically generating demonstrations in the absence of an expert. This is done by searching the state space using a modified version of RRT, a popular sampling-based planning algorithm. Once a success state is found, a trajectory leading from the initial state to this success state can be recovered from the RRT. Trajectories generated in this way are then distilled into policies through standard behaviour cloning.

Experimental results show that policies pre-trained with R3L-generated trajectories achieve faster learning and higher performance compared with both naive RL algorithms and sophisticated intrinsic motivation baselines. This holds even after accounting for the environment interactions incurred by the RRT phase. Additionally, R3L is more robust to initial conditions such as the random seed and the policy initialisation. While state-of-the-art RL methods are able to find good policies in some instantiations of the algorithms but not in others, R3L is able to solve the problem in all or most instances. Finally, R3L succeeds at solving difficult problems that other algorithms fail to learn altogether.



\subsection{Intrinsic Motivation from Bayesian Uncertainty}



Although behaviour cloning can greatly accelerate policy learning, the initial cloned policies still require an RL fine-tuning step to make them reliable and efficient. This phase tends to suffer considerable inefficiency when searching the state space for improved trajectories, as the algorithm must balance between exploration and exploitation.~\chapref{bayesian_curiosity} proposed Bayesian Curiosity, a method to explore the state space more efficiently by augmenting the reinforcement signal with an intrinsic reward. The state space is modelled as a Bayesian linear regression, with neural network features that are learned end-to-end from data. The uncertainty of this Bayesian model's prediction for any given state is used as a curiosity bonus to the standard reward. A standard RL algorithm trained with this modified reward will tend to seek out states that combine high environment reward and high uncertainty. The BLR is updated with every iteration of the RL algorithm, so that high uncertainty is correlated with states far from what has previously been observed, and the agent learns to seek out novel states.

Experiments on both simulated and physical tasks show that Bayesian Curiosity greatly enhances robustness and decreases sample complexity compared with both naive RL and state-of-the-art intrinsic motivation methods. Further, this improvement is present even after behaviour cloning has been applied to initialise the policies.

Of note is the fact that the hardware experiments were exceedingly time-consuming, taking a duration of several weeks to compare only two algorithms in a single setting. This in spite of the use of pre-training, which offers significant time savings as per~\chapref{variational_dropout}. Such a result belies the true difficulty of bringing reinforcement learning to physical systems, and suggests further exploration into combining learning from demonstration with intrinsic motivation.



\section{Future Work}
\label{sec:06:future_work}



In addition to the contributions described above, the work in this thesis also provides a foundation for further extension and research. The following section discusses a number of promising directions.



\subsection{RL for Planning and Planning for RL}



The method of Rapidly-exploring Random Reinforcement Learning, proposed in~\chapref{r3l}, partially bridges the gap between sampling-based planners and reinforcement learning. However, the coupling between these two fields is relatively weak, as the algorithm operates in separate phases of planning and RL. Further improvement can be achieved by making this coupling tighter.

One possibility is to perform more sophisticated learning while planning. The formulation of R3L as presented in this thesis only makes use of the successful trajectories discovered by the algorithm, and discards the remainder of the tree $\mathbb{T}$ that has been constructed. However, this remainder constitutes the majority of the tree, and contains a considerable amount of information about the transition dynamics of the corresponding MDP. There are a number of ways in which the discarded information could be exploited.

First, a more sophisticated local policy $\pi_l : \mathcal{S}\times\mathcal{S} \rightarrow \mathcal{A}$ could be learned from the transitions in $\mathbb{T}$, for example one represented by a neural network. Such an inverse dynamics model could be used to produce a curiosity signal, potentially in combination with Bayesian Curiosity, to help direct exploration in the planning phase, RL phase, or both. An alternative use for $\pi_l$ is as the policy of a goal-directed agent~\cite{bai2019guided}, potentially as part of a hierarchical model where a higher level policy generates local goals for the lower level policy, similar to the work of~\citet{levy2019learning}. The higher level policy can also be learned from data generated during planning.~\citet{Tamar19RSS} have recently proposed a neural motion planner, wherein a neural network model is trained to imitate a traditional sampling-based planner using RL. The result is a model that can generate a sequence of way-points connecting a start and goal state. Such a model, learned from the data in $\mathbb{T}$, can serve as a high-level policy in the hierarchical model proposed above: the neural motion planner proposes local goal states (the way-points), and the local policy steers the agent to those local goals.

In the opposite direction, there has been much work recently seeking to use the RL framework to replace parts of the RRT algorithm, such as the steering function and the expansion policy. However, the literature in this field is focused only on solving RRT problems, not MDPs. Such approaches can be combined with R3L and thus applied to MDPs, improving sample complexity in the R3L exploration phase. Furthermore, the RL policies learned during this phase can then be carried over to the RL fine-tuning phase, accelerating learning there as well. This creates a more tight coupling between the two domains, wherein RL is used to inform planning, which in turn is used to inform RL.

Finally, planning principles can be leveraged more explicitly during the execution of a reinforcement learning algorithm. As~\chapref{r3l} argues, one of the significant shortcomings of RL exploration is that it is unable to consider a global perspective and only examines the local perspective. Tree-based planners like RRT are more efficient in large part because they can choose any node in $\mathbb{T}$ from which to continue exploration, whereas RL algorithms have to start over from somewhere in the initial state distribution $p_0$ in every rollout, and during the rollout are restricted to exploring from the current state $s_t$. An alternative approach would involve an (off-policy) RL algorithm maintaining a tree or forest containing the history of trajectories. With some probability, the algorithm will choose a node in this forest from which to explore, instead of sampling from $p_0$ (at the start of a rollout) or choosing $s_t$ (in the middle of a rollout). The node from which to expand the forest can be chosen at random, or based on some criterion that would encourage efficient long-term exploration, such as Voronoi bias~\cite{lindemann2004incrementally} or intrinsic motivation.



\subsection{Well-Calibrated Uncertainty}



Guiding exploration based on uncertainty is a powerful method for ensuring that the amount of exploration an agent engages in is sufficient but not excessive. Naturally, if there is high uncertainty in some region of the state space, then there is information about that region that the model lacks. The downside of this approach is that it relies on having well-calibrated uncertainty estimates.

Gaussian Processes are able to achieve this characteristic in their predictive uncertainty, but scale poorly to high-dimensional or high-volume data. An alternative is sparse spectrum GPs, which approximate a full GP by means of a Bayesian linear regression with random Fourier features. The Bayesian Curiosity approach of~\chapref{bayesian_curiosity} can readily be modified to achieve this by adding random Fourier features to the existing BLR model.

Another possibility is to derive predictive uncertainty directly from neural networks. Recent work has shown that, by regularising to enforce certain conditions, it is possible to train an ensemble of neural networks so that the disagreement between members of the ensemble provides a well-calibrated estimate of the model uncertainty~\cite{lakshminarayanan2017simple}. If a policy is represented by such an ensemble, exploration can be guided by the uncertainty of the policy directly, rather than relying on the uncertainty of an auxiliary model, as is the common practice in intrinsic motivation algorithms. As an added benefit, models of this kind naturally extend to high-dimensional data such as images, with which Bayesian methods are known to struggle.

A deeper problem is the question of whether uncertainty used only as a reward signal is sufficient for trading off exploration and exploitation. Although uncertainty can be used to guide the agent towards under-explored regions of state space, it does not by itself give an indication of how much to explore or exploit at any given time. Bayesian approaches for estimating model uncertainty, in the vein of Bayesian Curiosity, could be combined with approaches from the Bayesian optimisation literature, such as Upper Confidence Bound~\cite{ucb}, in order to explicitly balance exploration with exploitation while maintaining applicability to continuous and high-dimensional problems.



\subsection{Decoupling Exploration from Exploitation}



Although R3L can find trajectories that successfully solve individual instances of an MDP, these trajectories are not robust to changes in the initial state or to stochasticity in the dynamics. To achieve a robust controller, the trajectories are distilled into a policy and fine-tuned with RL. This requires exploring the state space within the RL framework, which incurs high sample complexity in spite of the efficiency gains from R3L pre-training and intrinsic motivation. An alternative approach would be to delay the RL step further or circumvent it altogether by using trajectory optimisation methods to compute a robust and efficient controller.

For example, path integrals can be used to efficiently learn a policy that is robust to deviations from the demonstrated trajectories~\cite{chebotar2017path}. Similarly to behaviour cloning in RL, path integrals also require initial demonstrations, which can be provided by R3L exploration. Alternatively, successful trajectories which are not optimal (e.g. in terms of the trajectory return) can be optimised by algorithms such as CHOMP~\cite{zucker2013chomp}. Finally, in order to improve robustness, R3L exploration can be used to find actions or sequences of actions that connect off-trajectory states with existing trees. This approach would yield a tube of successful trajectories through state space. Learning to imitate such data will likely result in a policy that is better able to recover from trajectory deviations, so long as they are within the tube.



\subsection{Reinforcement Learning in Real Life}



The results presented in this thesis, and particularly the hardware experiments in~\chapref{bayesian_curiosity}, give an indication of how far reinforcement learning is from real world applicability in robotic systems. In spite of being restricted to only two algorithms in a single setting, the experiment of~\figref{05:real-control} took approximately 20 days to carry out. The causes for this excessive time cost are numerous, and each cause suggests possible directions for future work in order to make RL useful in physical real life applications.

Foremost, executing roll-outs on physical hardware is time consuming. Executions in simulation can be significantly faster than real time, and are easily parallelised by running multiple simulation instances in tandem. The drawback is that accurate simulators require significant engineering effort to construct, and often don't generalise between different systems. This can be addressed with methods from the rich literature on sim-to-real transfer~\cite{chebotar2019closing,golemo2018sim,james2019sim,peng2018sim,tan18sim}, allowing algorithms to train in imperfect simulations and then to transfer the learned policies to the real world with minimal performance loss. The sim-to-real approach lines up nicely with the work in~\chapref{r3l}: the R3L algorithm can be executed in simulation, which easily satisfies assumptions~\ref{as:04:1} and ~\ref{as:04:2}. Then, the resulting demonstrations can be used to bootstrap RL in simulation, before finally transferring to a real-world policy using sim-to-real techniques.

Another cause of long runtimes is the poor robustness exhibited by most RL algorithms. As seen throughout this thesis, the performance metrics of the baseline algorithms tend to have a very wide spread (in terms of e.g. standard deviation or inter-quartile range). In other words, different agents trained on the same task with the same algorithm vary significantly in the policies they manage to learn, due to stochasticity in the environment and policy initialisation. This causes difficulties both for research and for real-world development. In the research context, it means that experiments must be replicated a large number of times in order to obtain reliable results, which multiplies the total runtime of experiments. In industry, it means that developers may need to run the same learning algorithm several times to learn a useful policy. More robust algorithms, with a narrower spread in terms of the performance metrics, would greatly alleviate this problem.

As shown in~\chapref{r3l}, RL algorithms tend to behave like random walks in parameter space unless provided with a sufficiently informative reward signal, and such behaviour is a significant contributor to low robustness. This suggests further investigation of alternative approaches for exploration which do not suffer from random walk-like behaviour. The sampling-based motion planning literature offers several candidates that, like RRT, enjoy probabilistic completeness guarantees~\cite{gammell2015batch,li2015sparse,janson2015fast}. Some of these candidates also have other desirable attributes: Sparse Stable Trees~\cite{li2015sparse} are asymptotically optimal even in the absence of a steering function, Probabilistic Roadmaps~\cite{Kavraki1996} don't need to be reconstructed every time the initial state $s_0$ is changed, Rapidly-Exploring Random Vines~\cite{tahirovic2018rapidly} can quickly explore narrow passages in state space, and so forth. Looking beyond motion planning, algorithms from the search literature such as Levy flights~\cite{viswanathan1999optimizing} and iterative deepening search algorithms~\cite{korf1985depth} can be used for exploration in the absence of heuristic guidance. Monte Carlo Tree Search has famously been used to achieve groundbreaking RL results on the game of Go~\cite{alphago}, and the underlying principles could be combined with with an R3L-like approach to extend the applicability of MCTS to continuous domains.

Finally, the popularity of learning-from-scratch approaches also contributes to the high computational and time costs typical of RL algorithms. On the one hand, the ability to learn control policies from scratch is highly appealing, as it minimises the need for expert knowledge, engineering effort, or for making assumptions about the system. On the other hand, some amount of prior knowledge about the system is often available, and can be exploited to make learning faster. The result is a bifurcation of the literature, with one category of papers that consider only learning-from-scratch, another category that considers only learning from prior knowledge, and relatively few papers that consider how to combine both approaches.

The work in~\chapref{bayesian_curiosity} is a brief foray into this territory, and examines how intrinsic motivation algorithms, typically a learning-from-scratch methodology, perform when composed on top of learning-from-demonstration. However, the experiments are limited in this regard, and investigate only the addition of RL refinement to behaviour cloning. Further work is needed to understand how best to compose algorithms of the learning-from-scratch type with more sophisticated learning-from-demonstration algorithms, in order to get the best of both worlds. In particular, there is a need to examine whether various algorithms that improve RL exploration still retain their efficiency gains when combined with learning-from-demonstration. If many of the standard algorithms suffer from diminishing returns upon combination, this suggests the development of efficient exploration algorithms that are designed with demonstrations in mind, and account for the exploration that was already done in order to obtain those demonstrations.